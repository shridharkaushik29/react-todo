export interface ToDoType {
    id?: number,
    name?: string,
    done?: boolean,
    bucket?: string
}