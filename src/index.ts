import * as React from "react";
import * as ReactDOM from "react-dom";
import {AppComponent} from "./AppComponent";

ReactDOM.render(React.createElement(AppComponent), document.querySelector("body"));