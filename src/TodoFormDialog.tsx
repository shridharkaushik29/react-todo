import * as React from "react";
import {useState} from "react";
import {DialogProps} from "@material-ui/core/Dialog";
import {Button, Dialog, DialogActions, DialogContent, Grid, IconButton, TextField, Typography} from "@material-ui/core";
import {ToDoType} from "./types";
import {X} from "react-feather";

interface TodoFormDialogProps extends Partial<DialogProps> {
    model?: ToDoType,
    buckets: string[],
    onSave: (model: ToDoType) => void,
    onClose: () => void
}

export function TodoFormDialog(props: TodoFormDialogProps) {

    const {onClose, onSave, buckets, model = {}} = props;

    const [name, setName] = useState("");
    const [bucket, setBucket] = useState("");

    return <Dialog open={!!props.model} onClose={onClose} onEnter={() => {
        setName(model.name || "");
        setBucket(model.bucket || "");
    }}>
        <DialogContent>
            <Grid container alignItems="center">
                <Grid item xs>
                    <Typography variant="h5">
                        {model.id ? "Update" : "Create"} Todo
                    </Typography>
                </Grid>
                <IconButton onClick={onClose}>
                    <X/>
                </IconButton>
            </Grid>
            <Grid container direction="column">
                <datalist id="buckets">
                    {
                        buckets.map(bucket => <option key={bucket} value={bucket}/>)
                    }
                </datalist>
                <TextField autoFocus value={bucket}
                           onChange={e => setBucket(e.target.value)}
                           label={"Bucket"} inputProps={{
                    list: "buckets"
                }}/>
                <TextField value={name} onChange={e => setName(e.target.value)} label={"Name"} className="mt-3"/>
            </Grid>
        </DialogContent>
        <DialogActions>
            <Button onClick={() => {
                onSave({
                    id: model.id,
                    name, bucket
                });
                onClose();
            }} disabled={!name || !bucket} variant="contained" color={"primary"}>
                Submit
            </Button>
        </DialogActions>
    </Dialog>
}