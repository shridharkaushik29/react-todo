import * as React from "react";
import {useEffect, useState} from "react";
import {
    Button,
    Card,
    Divider,
    Grid,
    IconButton,
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    makeStyles,
    Tooltip,
    Typography
} from "@material-ui/core";
import clsx from "clsx";
import * as _ from "lodash";
import {Check, Edit, Trash, X} from "react-feather";
import {ToDoType} from "./types";
import {TodoFormDialog} from "./TodoFormDialog";

const useStyles = makeStyles({
    container: {
        width: "100vw",
        height: "100vh",
        backgroundColor: "#eee"
    },
    deletedText: {
        textDecoration: "line-through"
    }
});

export function AppComponent(props = {}) {

    const classes = useStyles(props);

    const [todo, setTodo] = useState<ToDoType>(undefined);

    const [todos, setTodos] = useState<ToDoType[]>([
        {
            id: 1,
            name: "Sample Todo",
            bucket: "Sample Bucket",
            done: true
        }
    ]);

    const addTodo = (model: ToDoType) => {
        setTodos(prevTodos => [
            ...prevTodos,
            {
                ...model,
                id: Math.random(),
            }
        ]);
    };

    const updateTodo = (model: ToDoType) => {
        setTodos(prevTodos => {
            const todos = [...prevTodos];
            const todo = todos.find(todo => model.id === todo.id);
            Object.assign(todo, model);
            return todos;
        });
    };

    const editTodo = (todo: ToDoType) => {
        setTodo(todo);
    };

    const toggleDone = ({id}: ToDoType) => {
        setTodos(prevTodos => {
            const todos = [...prevTodos];
            const todo = todos.find(todo => id === todo.id);
            todo.done = !todo.done;
            return todos;
        });
    };

    const deleteTodo = ({id}: ToDoType) => {
        setTodos(prevTodos => prevTodos.filter(todo => todo.id !== id));
    };

    useEffect(() => {
        setTodos(JSON.parse(localStorage.getItem("todos") || "[]"));
    }, []);

    useEffect(() => {
        localStorage.setItem("todos", JSON.stringify(todos))
    }, [todos]);

    return <Grid container className={clsx(classes.container)} alignItems="center" justify="center">
        <Grid container item xs={12} md={4}>
            <Card className="w-100">
                <Grid container alignItems="center" className="p-2-all">
                    <Grid item xs>
                        <Typography variant="h5">To-Do List</Typography>
                    </Grid>
                    <Grid item>
                        <Button onClick={() => setTodo({})} color="primary" variant="contained">Add New</Button>
                    </Grid>
                </Grid>
                <Divider/>
                <List>
                    {
                        todos.map(todo => <ListItem divider key={todo.id}>
                            <Grid container alignItems="center">
                                <ListItemText
                                    primary={<span className={clsx({
                                        [classes.deletedText]: todo.done
                                    })}>{todo.name}</span>}
                                    secondary={`Bucket - ${todo.bucket}`}
                                />
                                <ListItemSecondaryAction>
                                    <Tooltip title={todo.done ? "Mark Undone" : "Mark Done"}>
                                        {
                                            todo.done ?
                                                <IconButton onClick={() => toggleDone(todo)}>
                                                    <X size={15}/>
                                                </IconButton>
                                                :
                                                <IconButton onClick={() => toggleDone(todo)}>
                                                    <Check size={15}/>
                                                </IconButton>

                                        }
                                    </Tooltip>
                                    <Tooltip title={"Edit"}>
                                        <IconButton onClick={() => editTodo(todo)}>
                                            <Edit size={15}/>
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title={"Delete"}>
                                        <IconButton onClick={() => deleteTodo(todo)}>
                                            <Trash size={15}/>
                                        </IconButton>
                                    </Tooltip>
                                </ListItemSecondaryAction>
                            </Grid>
                        </ListItem>)
                    }
                    {
                        !todos.length &&
                        <ListItem>Nothing here yet. Click on "Add New" to add.</ListItem>
                    }
                </List>
            </Card>
        </Grid>
        <TodoFormDialog
            model={todo}
            buckets={_.uniq(todos.map(todo => todo.bucket))}
            onSave={model => model.id ? updateTodo(model) : addTodo(model)}
            onClose={() => setTodo(undefined)}
        />
    </Grid>
}